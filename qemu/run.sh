#!/bin/sh

SPICE_PORT=5924
SPICE_TITLE="Windows 10"
FILE="${HOME}/vm/win10.qcow2"
SAMBA="${HOME}/public"
VIRTIMG="${HOME}/vm/virtio-win-0.1.229.iso"

setup_qemu() {
	# Need to be run as root
	if [ "$EUID" -ne 0 ]; then
		echo "Permission denied"
		exit 1
	fi

	# Not necessary to setup bridge again
	if brctl show virtbr0 ; then
		echo "Bridge already set"
		exit 1
	fi

	brctl addbr virtbr0
	brctl addif virtbr0 enp0s31f6
	ip addr add dev virtbr0 192.168.50.200/24
	ip link set virtbr0 up
	iptables -I FORWARD -m physdev --physdev-is-bridged -j ACCEPT
	echo "Bridge set, ready to run vm"
}

start_qemu() {
	# Bridge needs to be set
	if ! brctl show virtbr0 ; then
		echo "Run again with command: $0 setup"
		exit 1
	fi

	# Prevent running qemu as root
	if [ "$EUID" -eq 0 ]; then
		echo "Running qemu as root is not allowed"
		exit 1
	fi

	# Keep -snapshot as the first option
	qemu-system-x86_64 -enable-kvm -daemonize \
		-snapshot \
		-cpu host,hv_relaxed,hv_spinlocks=0x1fff,hv_vapic,hv_time \
		-m 16G -smp 4 \
		-drive file=${FILE},if=virtio \
		-net nic,model=virtio,macaddr=52:54:11:11:11:01 -net bridge,br=virtbr0 \
		-device virtio-net,netdev=vmnic -netdev user,id=vmnic,smb=${SAMBA} \
		-vga qxl \
		-spice port=${SPICE_PORT},disable-ticketing=on \
		-usbdevice tablet \
		-device virtio-serial \
		-chardev spicevmc,id=vdagent,name=vdagent \
		-device virtserialport,chardev=vdagent,name=com.redhat.spice.0

	# Start spicy as soon as system has started booting
	exec spicy --title "${SPICE_TITLE}" 127.0.0.1 -p ${SPICE_PORT}
}

if [ $# -lt 1 ]; then
	echo "No arguments supplied"
	exit 1
fi

"$1_qemu"

# Old options for networking:
#-net nic -net user,hostname=windowsvm,id=nic0,smb=${SAMBA} \
#-netdev user,id=n0 -device virtio-net,netdev=n0 \
#-usb -netdev user,id=net0 -device usb-net,netdev=net0 \
#-device qemu-xhci,id=xhci \
#-device usb-host,bus=xhci.0,vendorid=0x0bda,productid=0x8153 \
