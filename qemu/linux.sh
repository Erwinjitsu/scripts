#!/usr/bin/sh

# Add distros to the list as you see fit
oslist="arch\ndebian11\nubuntu16\nubuntu20\n"
colours="-n #7da861 -N #282c35 -M #7da861 -m #282c35 -S #7da861 -s #282c35"

os="$(printf $oslist | wmenu $colours -i -p 'Pick OS: ')"

# Common settings - NOTE: iso and qcow files should start with same name
VM_FOLDER="$HOME/vm"
SPICE_TITLE="$os"
SPICE_PORT=5924
CD_ROM="$VM_FOLDER/iso/$os.iso"
IMG="$VM_FOLDER/qcow/$os.qcow2"

# Check if file exists
if [[ ! -e '$IMG' ]]; then
	echo "$IMG doesn't exists"
	echo "Create with:"
	echo "$ qemu-img create -f qcow2 20G $IMG"
	exit 1
fi

qemu-system-x86_64 -enable-kvm -daemonize \
	-cpu host -m 8G -smp 4 \
	-boot order=cd -drive file=${IMG},format=qcow2 \
	-vga qxl -device virtio-serial-pci \
	-spice port=${SPICE_PORT},disable-ticketing=on \
	-device virtserialport,chardev=spicechannel0,name=com.redhat.spice.0 \
	-chardev spicevmc,id=spicechannel0,name=vdagent \
	-cdrom ${CD_ROM}

exec spicy --title "${SPICE_TITLE}" 127.0.0.1 -p ${SPICE_PORT}
