#!/bin/sh

SPICE_PORT=5924
VIRTIMG="${HOME}/vm/virtio-win-0.1.229.iso"
IMG="${HOME}/vm/win10.qcow2"
DUMMY="${HOME}/vm/dummy.qcow2"

qemu-img create -f qcow2 ${DUMMY} 1G

qemu-system-x86_64 -daemonize -m 4G \
	-drive file=${IMG},if=ide \
	-drive file=${DUMMY},if=virtio \
	-cdrom ${VIRTIMG} \
    -spice port=${SPICE_PORT},disable-ticketing=on \
    -usbdevice tablet

spicy --title Windows 127.0.0.1 -p ${SPICE_PORT}

rm ${DUMMY}
