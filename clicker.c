/* 
 * Autoclicker with time interval and added random delay
 * Requirements: ydotool
 * TODO: No ydotool
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define YDOTOOL "ydotool"

int timed_clicker(int seconds, int count)
{
	int ret, i = 0;
	struct timespec remaining, request = { seconds, 0 };

	// Click the amount of times specified
	while (i < count)
	{
		// Some terminal output
		printf("(%d/%d) ", i + 1, count);
		fflush(stdout);

		// Random 140ms added on top
		request.tv_nsec = 850000000 + rand() % 140000000;
		ret = system(YDOTOOL " click 0xC0");
		if (ret != 0)
		{
			fprintf(stderr, "There was a problem executing " YDOTOOL "\n");
			return 1;
		}

		nanosleep(&request, &remaining);
		i++;
	}

	return 0;
}

int main(int argc, char *argv[])
{
	int seconds = 2, count = 50, c;
	char *countarg = NULL, *timearg = NULL;

	// Seed the random
	srand((unsigned int) (time(NULL)));

	while ((c = getopt(argc, argv, "c:t:")) != -1)
	{
		switch (c)
		{
		case 'c':
			countarg = optarg;
			break;
		case 't':
			timearg = optarg;
			break;
		}
	}

	if (countarg)
	{
		count = atoi(countarg);
	}
	if (timearg)
	{
		seconds = atoi(timearg);
	}

	return timed_clicker(seconds, count);
}
