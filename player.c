/*
 * player - Small wrapper that acts as a player from a user specified playlist
 * Copyright (C) 2023 Erwinjitsu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// The file given to this wrapper should have the absolute paths to any
// of the media files (mainly video files). This file is the playlist
// and user may add new lines with new absolute paths to media files at
// any given moment.

#include <stdio.h>
#include <string.h>

#define COMMAND_LENGTH 20
// TODO: Make more player options and let user decide it from CLI
#define PLAYER_OPTION 2


int playlist_next(char *file_path, int option)
{
	// Opens the next media file in playlist
	
	// Use the specified player
	char *player_command;
	switch (option) {
		case 1:
			player_command = "vlc --play-and-exit";
			break;
		case 2:
			player_command = "mpv --idle=no";
			break;
		default:
			return -1;
	}

	// The command string
	char command[COMMAND_LENGTH + strlen(file_path) + 1];
	sprintf(command, "%s %s", player_command, file_path);

	// The new stream
	FILE *stream = popen(command, "r");
	char buffer[255];	

	// Check the stream, as long as the stream is up we can stay here
	while (fgets(buffer, 255, (FILE *) stream)) {
		printf("%s", buffer);
	}

	// Close the stream and exit
	return pclose(stream);
}

int playlist_open(char *playlist_path, int old_count)
{
	// This open the playlist specified by user

	// Open the file with listed media locations
	FILE *playlist = fopen(playlist_path, "r");
	char line[255];	

	// Loop over each line in the file
	int count = 0;
	while (fgets(line, 255, (FILE *) playlist)) {
		count++;

		// Check if the count is larger than what we had before
		if (count > old_count) {
			int rc = playlist_next(line, PLAYER_OPTION);
			if (rc == -1) {
				printf("Oh no! Anyways..\n");
			}

			// Seek to the end so the while loop wraps
			fseek(playlist, 0, SEEK_END);
		}
	}

	// Close the file
	fclose(playlist);

	// If all the lines in the file are looped over, exit
	if (old_count == count) {
		return 0;
	}

	return count;
}

int main(int argc, char *argv[])
{
	// Check there's enough arguments
	if (argc < 2) {
		printf("You need to specify the playlist location\n");
		return(1);
	}

	// Loop for as long as user adds line (or last line is read)
	int old_count = 0;
	do {
		old_count = playlist_open(argv[1], old_count);
	} while (old_count);

	return 0;
}
