#!/usr/bin/sh

# Startup script in use with dwl. Usage:
# `dwl -s dwl-start.sh`

# Kill any previously running processes
if [ "$(pgrep -u $USER fnott)" ]; then
	kill -TERM $(pgrep -u "$USER" fnott)
fi
if [ "$(pgrep -u $USER swaybg)" ]; then
	kill -TERM $(pgrep -u "$USER" swaybg)
fi
if [ "$(pgrep -u $USER yambar)" ]; then
	kill -TERM $(pgrep -u "$USER" yambar)
fi

fnott >/dev/null 2>&1 &

# Start yambar for monitor(s)
( sleep 1; yambar -c ~/.config/yambar/monitor.yml ) >/dev/null 2>&1 &
#( sleep 1; yambar -c ~/.config/yambar/monitor2.yml ) >/dev/null 2>&1 &
#( sleep 1; yambar -c ~/.config/yambar/monitor3.yml ) >/dev/null 2>&1 &

picturedir="$HOME/Pictures/background/3m"

cd "$picturedir"
pic=$(ls | cut -c1-2 | sort -uR | tail -n1)

swaybg -o DP-1 -i "$pic"0.png -o DP-3 -i "$pic"1.png -o HDMI-A-1 -i "$pic"2.png >/dev/null 2>&1 &

# To cache lines from all monitors (21 / 7 = 3 monitors)
# to a cache file used by Yambar's dwl-module
while read line; do
	i=$((i+1))
	if (( $i > 21 )); then
		i=1
		true > ~/.cache/dwltags
	fi
	echo $line >> ~/.cache/dwltags
done < "${1:-/dev/stdin}"
