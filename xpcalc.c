/*
 * Runescape xp calculator
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

int calcXPDiff(int startlvl, int endlvl)
{
	int xp = 0, lvl;

	if (startlvl < 1 || endlvl < 1)
	{
		return -1;
	}

	if (endlvl < startlvl)
	{
		return -2;
	}

	for (lvl = startlvl; lvl < endlvl; lvl++)
	{
		xp += lvl + 300 * pow(2.0, lvl / 7.0);
	}

	return xp / 4;
}

int main(int argc, char *argv[])
{
	int startlvl = 1, endlvl = 2, c;
	int xp, ret = EXIT_SUCCESS;

	if (argc < 2)
	{
		fprintf(stderr, "no arguments given: defaulting to smallest\n");
	}

	while ((c = getopt(argc, argv, "s:e:")) != -1)
	{
		switch (c)
		{
		case 's':
			startlvl = atoi(optarg);
			break;
		case 'e':
			endlvl = atoi(optarg);
			break;
		}
	}

	xp = calcXPDiff(startlvl, endlvl);

	if (xp > -1)
	{
		printf("XP needed for %d: %d\n", endlvl, xp);
	}
	else
	{
		ret = xp;
	}

	return ret;
}
