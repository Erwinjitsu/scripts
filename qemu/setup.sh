#!/bin/sh

SPICE_PORT=5924
WINIMG="${HOME}/vm/Win10_22H2_EnglishInternational_x64.iso"
VIRTIMG="${HOME}/vm/virtio-win-0.1.229.iso"
IMG="${HOME}/vm/win10.qcow2"

qemu-system-x86_64 --enable-kvm -daemonize \
	-drive driver=qcow2,file=${IMG},if=virtio -m 6144 \
	-net nic,model=virtio -net user -cdrom ${WINIMG} \
	-drive file=${VIRTIMG},index=3,media=cdrom \
	-rtc base=localtime,clock=host -smp cores=4,threads=8 \
	-spice port=${SPICE_PORT},disable-ticketing=on \
	-usb -device usb-tablet \
	-net user,smb=$HOME
exec spicy --title Windows 127.0.0.1 -p ${SPICE_PORT}
