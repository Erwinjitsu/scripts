#!/usr/bin/sh

picturedir="$HOME/Pictures/background/3m"

# Kill any previously running processes
if [ "$(pgrep -u $USER swaybg)" ]; then
	kill -TERM $(pgrep -u "$USER" swaybg)
fi

cd "$picturedir"

pic=$(ls | cut -c1-2 | sort -uR | tail -n1)

swaybg -o DP-1 -i "$pic"0.png -o DP-3 -i "$pic"1.png -o HDMI-A-1 -i "$pic"2.png
