/*
 * dwl-tags - Script used for yambar with dwl as the compositor
 * Copyright (C) 2023 Erwinjitsu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// The monitor name given to this script should be that of specified in
// yambar config.yml (bar.monitor). You need to launch dwl with command
// that caches stdout from dwl.
//
// Example: 
// `/usr/bin/sh -c 'dwl | while read line; do
//  	i=$((i+1))
//
//	# 21 is the amount of lines we want to save
//	# 7x amount of monitors
//  	if (( $i > 21 )); then
//  		i=1
//  		true > ~/.cache/dwltags
//  	fi
//  
//  	echo $line >> ~/.cache/dwltags
//  done`

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <pwd.h>

#define TAG_AMOUNT 8


static void name_parse(char *pointer)
{
	// Parse the line's name and print information
	
	if (strncmp(pointer, "tags", 4) == 0) {
		pointer += 5;

		int masks[4], cut;
		for (int i = 0; i < 4; i++) {
			while (isspace(pointer[0])) {
				pointer++;
			}
			
			cut = 0;
			while (isspace(pointer[cut]) == 0) {
				cut++;
			}

			char tmp[cut];
			strncpy(tmp, pointer, cut);
			masks[i] = atoi(tmp);
			pointer += cut;
		}
		
		unsigned char mask;
		for (int i = 0; i < TAG_AMOUNT; i++) {
			mask = 1 << i;
			
			printf("tag_%d|string|%d\n", i, i + 1);
			printf("tag_%d_focused|bool|%s\n", i,
				(masks[1] & mask) ? "true" : "false");
			printf("tag_%d_occupied|bool|%s\n", i,
				(masks[0] & mask) ? "true" : "false");
		}
	} else if (strncmp(pointer, "title", 5) == 0 && pointer[6] != 0) {
		pointer += 6;
		printf("title|string|%s", pointer);
	} else if (strncmp(pointer, "layout", 6) == 0) {
		pointer += 7;
		printf("layout|string|%s\n", pointer);
	}
}

static int line_parse(char *file_path, char *display)
{
	// Parse the line for given display

	FILE *cache = fopen(file_path, "r");
	size_t length = 255;
	char line[length], *pointer;
	int display_set = 0;

	// Get the right display's information
	pointer = line;
	while (getline(&pointer, &length, cache) != -1 &&
		display_set != -1) {
		pointer += strlen(display) + 1;
		if (strncmp(display, line, strlen(display)) == 0) {
			display_set = 1;
			name_parse(pointer);
		} else if (display_set) {
			display_set = -1;
		}
		pointer = line;
	}
	
	return fclose(cache);
}

int main(int argc, char *argv[])
{
	// Check there's enough arguments
	if (argc < 2) {
		fprintf(stderr, "%s: missing required arguments\n",
			argv[0]);
		return 1;
	}

	// Check the cache file exists
	struct passwd *pw = getpwuid(getuid());
	const char *home_dir = pw->pw_dir;

	char file_path[strlen(home_dir) + 20];
	sprintf(file_path, "%s/.cache/dwltags", home_dir);
	if (access(file_path, F_OK) != 0) {
		fprintf(stderr, "File %s does not exist\n", file_path);
		return 1;
	}

	// Print out tag information and exit
	return line_parse(file_path, argv[1]);
}
